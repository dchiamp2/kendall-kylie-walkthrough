#Kendall-Kylie Product Images Walkthroiugh

##Step 0: Log In
* Log in to the Shopify backend with your credentials through shop.kendall-kylie.com/admin

##Step 1: Upload Image
* Navigate to the products section from the left nav bar, click on 'Products'

* Select the product you want to update

* In the 'Images' section, click 'Add Images' to upload an image from your computer

* Or, click the "Add image from URL" to upload an image from the web


	NOTE: you can upload multiple photos at once from your computer

##Step 2: Change Hover Image
* The Hover Image corresponds to the **FIRST** Product Variant Image 

* Click the image icon on the first product variant, and select the uploaded image you wish to be the hover

* This Image will now be the hover state on the product grid.

##Step 3: Change Product Detail Images
* At the bottom of the left nav bar, click on Apps, and then select the 'Metafields2' app.

* Within Metafields2, select  'Products'.

* Click on the product you wish to edit (click on the '+'' symbol)

* The 'colorVariant' numbers correspond directly to the order of variants seen on the product page

* Copy the image url that you previously uploaded by viewing that image in the CMS, right clicking it and selecting 'Copy Image Address'

* Paste this url in to the corresponding 'colorVariant' field in the 'Metafields2' app.

* The order of the images is the same order as the urls in the 'colorVariant' field.

* They must be seperated by commas, and start with '//' (**NOT http or https**).

* Remove '_grande' from image url for higher quality (if present)

* EXAMPLE: if 'Bright-White' is the first color variant you see in the product CMS page, and you want to add a white detail image,  you would paste the url of the new image in the 'colorVariant1' field.

	Note: The order is very important! 


